﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class PlayerMovement : MonoBehaviour
{
    AudioSource player_audio;

    public CharacterController controller;
    public AudioClip page_get;
    public AudioClip cave_get;
    public Slider healthbar;
    public Text pg_keepr;

    public Keymaster keymaster;

    public float max_hp = 100;
    public float hp;
    public float speed = 12f;
    public int pages_left;


    private void Start()
    {
        pages_left = 7;
        hp = max_hp;
        player_audio = GetComponent<AudioSource>();

        Cursor.visible = false;
    }

    void Update()
    {
        float x = Input.GetAxis("Horizontal");
        float z = Input.GetAxis("Vertical");

        healthbar.value = hp;

        if(pages_left > 0)
        {
            pg_keepr.text = string.Format("Pages left: {0}", pages_left);
        }
        if(pages_left <= 0)
        {
            pg_keepr.text = "Get to\nthe cave!";
            keymaster.dana = false; // Begins the ending!

        }
        


        Vector3 move = transform.right * x + transform.forward * z;

        controller.Move(move * speed * ((hp + (100f - hp)) * .01f) * Time.deltaTime);
        // The lower your health, the faster your ghost!
        
        if(transform.position.y > 1.08f)
        {
            transform.position = new Vector3(transform.position.x, 1.08f, transform.position.z);
        }

        if(hp > max_hp)
        {
            hp = max_hp;
        }
        if(hp <= 0)
        {
            SceneManager.LoadScene(sceneName: "GameOverScene");
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
            
        }

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Page"))
        {
            if (pages_left == 1)
            {
                player_audio.PlayOneShot(cave_get, 0.25f);
            }
            else
            {
                player_audio.PlayOneShot(page_get, 0.5f);
            }
            hp = hp + 10f; // HP boost for collecting a page!
            pages_left = pages_left - 1;
            
        }
    }
}
