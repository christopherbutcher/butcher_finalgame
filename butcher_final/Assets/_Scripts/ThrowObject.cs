﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThrowObject : MonoBehaviour
{
    public PlayerMovement player;

    public GameObject projectile;
    public AudioClip shootSound;
    // variable type referring to any audio file.

    private float throwSpeed = 2000f;
    private AudioSource source;
    private float volLowRange = 0.25f;
    private float volHiRange = 0.75f;
    // sets volume limits to have a range
    //      of sounds

    public bool interactive_obj;

    // Start is called before the first frame update
    void Awake()
    {
        source = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            if (interactive_obj == false)
            {
                float vol = Random.Range(volLowRange, volHiRange);
                // our volume is now random between our two parameters

                source.PlayOneShot(shootSound, vol);
                // PlayOneShot plays once, takes a sound clip and a volume.
                

                GameObject throwThis = Instantiate(projectile, transform.position, transform.rotation);
                //create new game obj. instantiate a projectile at / with this obj's pos & rotation.

                throwThis.GetComponent<Rigidbody>().AddRelativeForce(new Vector3(0, 0, throwSpeed));

                if(player.hp > 5)
                {
                    player.hp = player.hp - 5;
                }
                if(player.hp <= 5)
                {
                    if(player.hp > 1)
                    {
                        player.hp = 1;
                    }
                    else
                    {
                        player.hp = 0;
                    }
                    
                }
                

                
            }
            if (interactive_obj == true)
            {
                print("CLICK CLACK!");
            }
            
            
        }
    }



}
