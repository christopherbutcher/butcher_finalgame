﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class PowerOff : MonoBehaviour
{
    public float time_til_end;

    public AudioSource quake_sound;

    // Start is called before the first frame update
    void Start()
    {
        time_til_end = 0;
    }

    // Update is called once per frame
    void Update()
    {
        //time_til_end += Time.deltaTime;

        time_til_end += 1;
        
        if (time_til_end >= 1500) // The frames in the final scene.
        {
            Application.Quit();
            //print("game over man!!");
        }
        /*else
        {
            print(time_til_end);
        }*/

    }

    void Quake()
    {
        quake_sound.Stop();
    }
}
