﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fader : MonoBehaviour
{
    public float fadeDuration = 1f;
    public float displayImageDuration = 1f;
    public CanvasGroup black_screen;
    public CanvasGroup smashcut;
    public bool game_on;
    public bool game_over;
    float m_Timer;


    // Start is called before the first frame update
    void Start()
    {
        game_on = false;
        m_Timer = 0;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
       if(game_on == false)
        {
            m_Timer += Time.deltaTime;
            black_screen.alpha = fadeDuration - m_Timer;
        }
        if (black_screen.alpha == 0)
        {
            game_on = true;
            m_Timer = 0;
        }
        if(game_over == true)
        {
            if(black_screen.alpha < 1)
            {
                m_Timer += Time.deltaTime;
                black_screen.alpha = m_Timer;
            }
            else
            {
                black_screen.alpha = 1;
                game_over = false; 
            }
            
        }
    }
}
