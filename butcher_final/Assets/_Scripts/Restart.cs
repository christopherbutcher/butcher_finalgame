﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Restart : MonoBehaviour
{
    public CanvasGroup a_txt;
    public float alf;
    // Hey, listen, I don't know if you know this, but start is
    // called before the first frame update. Just fyi.
    void Start()
    {
        Cursor.visible = false;
        a_txt.alpha = -1;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Return))
        {
            SceneManager.LoadScene(sceneName: "SampleScene");
        }
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }
        if(alf < 1)
        {
            alf += (Time.deltaTime / 2);
        }
        else
        {
            alf = 1;
        }


        a_txt.alpha = alf;
    }
}
