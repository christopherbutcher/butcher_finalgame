﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AOE : MonoBehaviour
{
    
    public float charging; // time left in the charge
    CapsuleCollider rad;
    AudioSource source;
    public List<Baddies> baddies_pool = new List<Baddies>();
    public PlayerMovement player;
    public Slider slider;
    public AudioClip aoe_sound;

    public ParticleSystem wisp;

    private void Start()
    {
        charging = 30;
        rad = GetComponent<CapsuleCollider>();
        source = GetComponent<AudioSource>();
        
    }

    void Update()
    {
        slider.value = (30f - charging);

        if ((charging <= 0f) && (Input.GetKeyDown("space")))
        {
            source.PlayOneShot(aoe_sound);
            //print("OK!!!");
            // transform.localScale = new Vector3(6f, 0.9f, 6f);
            rad.radius = 20;
            charging = 30f;

            wisp.Play();

            if (player.hp > 1)
            {
                player.hp = (player.hp / 2);
            }

        }

        if (charging > 0f)
        {
            charging -= 1f * Time.deltaTime;
        }
        if (rad.radius > 0)
        {
            //transform.localScale = transform.localScale - new Vector3(.025f, 0f, .025f);
            rad.radius -= 1.5f * Time.deltaTime; //?
        }
        if(rad.radius <= 0)
        {
            wisp.Stop();
        }

        foreach (Baddies meanie in baddies_pool)
        {
            print("AHHH!");
            meanie.terrified = true;
        }

    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Baddies"))
        {
            if(charging > 0)
            {
                print("boo!");
                baddies_pool.Add(other.GetComponent<Baddies>());
                other.GetComponent<Baddies>().terrified = true;
                other.GetComponent<Baddies>().scare_time = Random.Range(200, 500);
                //other.GetComponent<AudioSource>().PlayOneShot(other.GetComponent<Baddies>().s_scream, Random.Range(1f, 1.75f));
            }
            
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Baddies"))
        {
            print("bye!");
            baddies_pool.Remove(other.GetComponent<Baddies>());
        }
    }
}
