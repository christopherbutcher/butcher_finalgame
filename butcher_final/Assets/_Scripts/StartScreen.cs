﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class StartScreen : MonoBehaviour
{
    public bool on_opts;
    public bool on_cred;

    public CanvasGroup controlScreen;
    public CanvasGroup creditsScreen;


    // Start is called before the first frame update
    void Start()
    {
        on_cred = false;
        on_opts = false;
    }

    // Update is called once per frame
    void Update()
    {

    }
    
    public void StartGame()
    {
        SceneManager.LoadScene(sceneName: "SampleScene");
        // Starts the game!
    }

    public void OptionsScreen()
    {
        controlScreen.GetComponent<CanvasGroup>().alpha = 1f;
        creditsScreen.GetComponent<CanvasGroup>().alpha = 0f;

    }

    public void CreditsScreen()
    {
        controlScreen.GetComponent<CanvasGroup>().alpha = 0f;
        creditsScreen.GetComponent<CanvasGroup>().alpha = 1f;
    }
}
