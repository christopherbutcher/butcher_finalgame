﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseLook : MonoBehaviour
{

    public float mouseSensitiviy = 100f;

    public Transform player;

    float xRotate = 0f;

    // Start is called before the first frame update
    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
    }

    // Update is called once per frame
    void Update()
    {
        float mouse_x = Input.GetAxis("Mouse X") * mouseSensitiviy * Time.deltaTime;
        float mouse_y = Input.GetAxis("Mouse Y") * mouseSensitiviy * Time.deltaTime;

        xRotate -= mouse_y;
        xRotate = Mathf.Clamp(xRotate, -90f, 90f);

        transform.localRotation = Quaternion.Euler(xRotate, 0f, 0f);

        player.Rotate(Vector3.up * mouse_x);

    }
}
