﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Baddies : MonoBehaviour
{
    public GameObject aoe; // The AOE of the Player
    public GameObject player; // The Player itself.
    public AudioSource scary_sounds;

    Animator m_Animator;
    Rigidbody m_Rigidbody;
    Vector3 m_Movement;
    Quaternion m_Rotation = Quaternion.identity;
    AudioSource bad_sounds;

    public AudioClip s_walk;
    public AudioClip s_run;
    public AudioClip s_gasp;
    public AudioClip s_scream;

    public NavMeshAgent navMeshAgent;

    public Transform[] waypoints;
    // a public array of transforms (called waypoints!)
    int m_CurrentWaypointIndex;
    // stores current index of the waypoint array

    public List<GameObject> ghostbusting = new List<GameObject>();
    // This is the list of players (which should only 
    //      ever be empty or have one item in it).

    public bool terrified;
    public bool spooked;
    public bool walking;
    public bool at_point;
    public bool radio;

    public int idle_time;
    public int scare_time;
    //int length;

    bool see_ghost; // For if the player is in flashlight range.
    Vector3 dirToPlayer; // the direction of the player
    public float dist2player; // the distance to the player
    public float turnSpeed = 80f;
    

    void Start()
    {
        bad_sounds = GetComponent<AudioSource>();
        m_Animator = GetComponent<Animator>();
        m_Rigidbody = GetComponent<Rigidbody>();

        //length = ghostbusting.Count; // <---- don't need?
        // Counts the number of items in the list
        //        of players the enemy can see.

        walking = false;
        at_point = true;
        see_ghost = false;

        idle_time = Random.Range(10,100);

        dirToPlayer = player.transform.position - transform.position;
        dirToPlayer = dirToPlayer.normalized;
        float dotProduct = Vector3.Dot(dirToPlayer, transform.forward);

        dist2player = Vector3.Distance(player.transform.position, transform.position);
    }

    void FixedUpdate()
    {
        m_Animator.SetBool("IsWalking", walking);
        m_Animator.SetBool("at_point", at_point);
        m_Animator.SetBool("terrified", terrified); 
        m_Animator.SetBool("spooked", spooked);
        //m_Animator.SetBool("running", running);

        PlayAudio();

        dirToPlayer = player.transform.position - transform.position;
        dirToPlayer = dirToPlayer.normalized;
        float dotProduct = Vector3.Dot(dirToPlayer, transform.forward);
        //print(dotProduct);
        dist2player = Vector3.Distance(player.transform.position, transform.position);

        if (terrified == false)
        {
            if(spooked == false)
            {
                if((dist2player > 7) || (dotProduct <= 0)) 
                {
                    see_ghost = false;

                    if (navMeshAgent.remainingDistance <= navMeshAgent.stoppingDistance)
                    {
                        at_point = true;
                        walking = false;
                    }

                    if ((at_point == true) && (idle_time > 0))
                    {
                        idle_time -= 1;
                    }

                    if ((idle_time <= 0) && (at_point == true))
                    {
                        at_point = false;
                    }

                    if (at_point == false)
                    {
                        walking = true;

                        navMeshAgent.SetDestination(waypoints[m_CurrentWaypointIndex].position);
                    }
                }
                if((dist2player <= 7) && (dotProduct >= 0.5f)) //
                {
                    if(dist2player > 2)
                    {
                        navMeshAgent.SetDestination(player.transform.position); // CHASE THE PLAYER
                        at_point = false;
                        walking = true;
                    }
                    if(dist2player <= 2)
                    {
                        navMeshAgent.SetDestination(transform.position); // Stops the teens in their tracks.
                        at_point = true;
                        walking = false;
                    }
                    
                    
                    if(dotProduct >= 0.75f) 
                    {
                        if (Vector3.Distance(transform.position, player.transform.position) <= 5f) //
                        {
                            //print("DotProduct: " + dotProduct);
                            see_ghost = true;
                            player.GetComponent<PlayerMovement>().hp -= 1f; //
                            // DAMAGE
                            //print("DAMAGED");
                        }
                    }
                }
            }

            if(spooked == true)
            {
                navMeshAgent.SetDestination(transform.position);

                

                if(scare_time > 0)
                {
                    scare_time -= 1;
                }
                if(scare_time <= 0)
                {
                    spooked = false;
                    navMeshAgent.SetDestination(waypoints[m_CurrentWaypointIndex].position);
                }
                walking = false;
                at_point = false;

            }


        }
        if(terrified == true)
        {
            walking = true;
            at_point = false;


            if (scare_time > 0)
            {
                scare_time -= 1;

                m_CurrentWaypointIndex = (m_CurrentWaypointIndex + 1) % waypoints.Length;
                
                navMeshAgent.SetDestination(waypoints[m_CurrentWaypointIndex].position);
            }
            if(scare_time <= 0)
            {
                navMeshAgent.SetDestination(transform.position);
                terrified = false;
                at_point = true;
                walking = false;
            }
        }


    }


    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject == aoe)
        {
            scary_sounds.PlayOneShot(s_scream, Random.Range(.5f,1f));
        }
        if(other.CompareTag("Orb"))
        {
            if (!terrified)
            {
                scary_sounds.Stop();
                scary_sounds.PlayOneShot(s_gasp, Random.Range(.5f, 1f));
            }
        }
    }

    void PlayAudio()
    {
        if (walking)
        {
            if (bad_sounds.clip != s_walk)
            {
                bad_sounds.Stop();
                bad_sounds.clip = s_walk;

            }
            if (!bad_sounds.isPlaying)
            {
                bad_sounds.Play();
            }
        }
        else if(terrified)
        {

            if (bad_sounds.clip != s_run)
            {
                bad_sounds.Stop();
                bad_sounds.clip = s_run;
            }
            if (!bad_sounds.isPlaying)
            {
                bad_sounds.Play();
            }
        }
        else
        {
            bad_sounds.Stop();     
        }
    }
}
