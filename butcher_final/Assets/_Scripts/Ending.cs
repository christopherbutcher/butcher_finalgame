﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ending : MonoBehaviour
{
    AudioSource source;
    int step;

    public AudioSource rumble;
    public AudioSource rock_sounds;

    public AudioClip rock1;
    public AudioClip rock2;
    public AudioClip rock3;
    public AudioClip rock4;




    // Start is called before the first frame update
    void Start()
    {
        Cursor.visible = false;
        source = GetComponent<AudioSource>();
        step = 0;
    }


    void MakeNoise()
    {
        rumble.Play();
    }

    void PlayMusic()
    {
        if (!source.isPlaying)
        {
            source.Play();
        }

    }

    void Rock1()
    {
        rock_sounds.PlayOneShot(rock1);
    }
    void Rock2()
    {
        rock_sounds.PlayOneShot(rock1);
    }
    void Rock3()
    {
        rock_sounds.PlayOneShot(rock1);
    }
    void Rock4()
    {
        rock_sounds.PlayOneShot(rock2);
    }
    void Rock5()
    {
        rock_sounds.PlayOneShot(rock2);
    }
    void Rock6()
    {
        rock_sounds.PlayOneShot(rock1);
    }
    void Rock7()
    {
        rock_sounds.PlayOneShot(rock2);
    }
    void Rock8()
    {
        rock_sounds.PlayOneShot(rock3);
    }
    void Pillar()
    {
        rock_sounds.PlayOneShot(rock4);
    }


}
