﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrashSounds : MonoBehaviour
{
    public AudioClip crashSoft;
    public AudioClip crashHard;

    private AudioSource source;
    private float lowPitchRange = 0.75f;
    private float highPitchRange = 1.25f;
    private float velToVol = .2f;
    private float velocityClipSplit = 10f;

    void Awake()
    {
        source = GetComponent<AudioSource>();
    }

    private void OnCollisionEnter(Collision collision)
    {
        float hitVol = collision.relativeVelocity.magnitude * velToVol;
        // scaling our velocity by .2f so we can use it for volume.
        // explosion loud, settling soft!!!

        source.PlayOneShot(crashSoft, 1f);

        if (collision.relativeVelocity.magnitude < velocityClipSplit)
            source.PlayOneShot(crashSoft, hitVol);
        else
            source.PlayOneShot(crashHard, hitVol);
    }
}
