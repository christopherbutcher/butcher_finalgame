﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Keymaster : MonoBehaviour
{
    public bool dana;
    public bool zuul;

    public Light lite;
    public AudioClip spooky_sounds;

    // Start is called before the first frame update
    void Start()
    {
        zuul = false;
        dana = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (!dana && !zuul)
        {
            print("There is no Dana, only Zuul!");
            GetComponent<MeshCollider>().enabled = false;
            GetComponent<MeshRenderer>().enabled = false;
            lite.enabled = true;
            zuul = true;
        }
    }
}
