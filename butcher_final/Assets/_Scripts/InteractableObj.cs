﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractableObj : MonoBehaviour
{
    public bool is_on;
    public GameObject player;

    public ParticleSystem fire;
    public AudioClip song;
    AudioSource source;

    // Start is called before the first frame update
    void Start()
    {
        source = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if (this.CompareTag("Turntable"))
        {

        }
        if (this.CompareTag("Radio"))
        {

        }
    }

    private void OnTriggerEnter(Collider other)
    {
         
    }
    private void OnTriggerExit(Collider other)
    {
        
    }

}
