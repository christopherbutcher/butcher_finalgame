﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Waypoints : MonoBehaviour
{
    public GameObject robin;
    public float wX;
    public float wZ;
    // Start is called before the first frame update
    void Start()
    {
        wX = Random.Range(-11.8f, 100.85f);
        wZ = Random.Range(-7.5f, 30.75f);
        transform.position = new Vector3(wX, 1f, wZ);
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        //print("ONTRIGGERENTER");
        if (other.gameObject == robin)
        {
            //print("OH OK");
            wX = Random.Range(-11.8f, 100.85f);
            wZ = Random.Range(-7.5f, 30.75f);
            transform.position = new Vector3(wX, 1f, wZ);
            //print("BADDIES GOT TO WAYPOINT");
            
            other.GetComponent<Baddies>().at_point = true;
            other.GetComponent<Baddies>().idle_time = Random.Range(42, 666); // <-------- change variables??
            // Talks to Baddies script.

        }
    }

    

}
