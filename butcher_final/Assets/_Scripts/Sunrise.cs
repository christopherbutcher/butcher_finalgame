﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sunrise : MonoBehaviour
{
    float rps = -0.0006979168f;
    // This is very specific!

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(rps, 0f, 0f);
    }
}
