﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Orb : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Baddies"))
        {
            other.GetComponent<AudioSource>().PlayOneShot(other.GetComponent<Baddies>().s_gasp, Random.Range(1f, 1.75f));
            other.GetComponent<Baddies>().spooked = true;
            other.GetComponent<Baddies>().scare_time = Random.Range(50, 250); // Update range?
            //print("Scared 'em!");
        }
        Destroy(this.gameObject);
        //print("Bye bullets!");
    }
}
